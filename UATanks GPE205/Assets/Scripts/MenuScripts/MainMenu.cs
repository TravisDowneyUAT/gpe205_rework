﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    /*begins the game by loading the next scene in the build index*/
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        AudioManager.instance.PlaySound("ButtonClick");
    }

    /*quits the game once the button is pressed*/
    public void QuitGame()
    {
        Application.Quit();
        AudioManager.instance.PlaySound("ButtonClick");
        Debug.Log("Quitting game");
    }
}
