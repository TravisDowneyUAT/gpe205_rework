﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class OptionsMenu: MonoBehaviour
{
    public AudioMixer soundFXMixer; //reference to the FX sound mixer
    public AudioMixer musicMixer; //reference to the music soud mixer
    public GameManager manager; //reference to the game manager
    public Slider fxSlider; //reference to the slider that controls the FX volume
    public Slider musicSlider; //reference to the slider that controls the music volume

    /*Changing the volume FX in game based on the value of the slider in the options menu*/
    public void SetFXVolume(float volume)
    {
        soundFXMixer.SetFloat("fxvolume", volume);
    }

    /*Changing the volume of the music in game based on the value of the slider in the options menu*/
    public void SetMusicVolume(float volume)
    {
        musicMixer.SetFloat("musicvolume", volume);
    }

    /*Setting the multiplayer variable to true or false based on toggle in options menu*/
    public void SetMultiplayer(bool isMultiplayer)
    {
        manager.multiPlayer = isMultiplayer;
    }
    
    /*Setting the map of the day variable to true or false based on toggle in options menu*/
    public void SetMapOfDay(bool isMapOfDay)
    {
        manager.isMapOfTheDay = isMapOfDay;
    }
}
