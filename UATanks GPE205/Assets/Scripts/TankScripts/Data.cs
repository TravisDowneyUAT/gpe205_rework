﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour
{
    public float moveSpeed; /*float variable holding tank movement speed*/
    public float turnSpeed; /*float variable holding tank turning speed*/
    public float fireForce; /*float variable holding the firing force of shells*/
    public float shootDelay; /*float varibale holding the time between shots*/
    public float damage; /*float variable holding the damage a shell can do to tanks*/
    public Rigidbody shell; /*reference to the rigidbody component of the shell*/
    public float startHealth = 100f; /*float variable holding the start health of the tanks*/
    public float moveVol; /*float variable holding the movement volume of the tank*/
    public float turnVol; /*float variable holding the turning volume of the tank*/

    [HideInInspector]
    public TankMovement motor; /*refernence to the TankMovement script*/
    [HideInInspector]
    public ShootingShells shooting; /*reference to the ShootingShells sript*/
    [HideInInspector]
    public TankHealth healthInfo; /*reference to the TankHealth script*/

    // Start is called before the first frame update
    void Start()
    {
        motor = GetComponent<TankMovement>(); /*Setting the reference to the TankMovement script as the script on the GameObject*/
        shooting = GetComponent<ShootingShells>(); /*Setting the reference to the ShootingShells script as the script on the GameObject*/
        healthInfo = GetComponent<TankHealth>(); /*Setting the reference to the TankHealth script as the script on the GameObject*/
    }
}
