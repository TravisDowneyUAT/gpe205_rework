﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingShells : MonoBehaviour
{
    [HideInInspector]
    public Data player1Data;
    [HideInInspector]
    public Data player2Data;

    public Rigidbody shell; //reference to the rigidybody of the shell
    public Transform firePoint; //reference to the fire point of the tank
    private float nextShot = 0.0f; //float variable measuring the time until the player can fire again

    void Start()
    {
        player1Data = GameObject.FindGameObjectWithTag("Player1").GetComponent<Data>();
        player2Data = GameObject.FindGameObjectWithTag("Player2").GetComponent<Data>();
    }

    /*Shoot() method which spawns in the shell as a rigidbody and fires it forward from
     the tanks fire point*/
    public void Shoot()
    {
        if (Time.time > nextShot)
        {
            Rigidbody shellFire =
            Instantiate(shell, firePoint.position, firePoint.rotation) as Rigidbody;
            shellFire.velocity = player1Data.fireForce * firePoint.forward;
            shellFire.velocity = player2Data.fireForce * firePoint.forward;

            ShellDamage shellData = shell.GetComponent<ShellDamage>();
            shellData.damage = player1Data.damage;
            shellData.damage = player2Data.damage;
            shellData.fireForce = player1Data.fireForce;
            shellData.fireForce = player2Data.fireForce;

            nextShot = Time.time + player1Data.shootDelay;
            nextShot = Time.time + player2Data.shootDelay;
        }

        AudioManager.instance.PlaySound("BulletFire");
    }
}
