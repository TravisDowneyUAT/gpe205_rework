﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    public float vol; /*float variable holding the volume of the tanks sound*/
    public float reducePerFrame; /*float variable holding the amount to reduce the volume per frame*/

    // Update is called once per frame
    void Update()
    {
        /*if statement checking to see that if the volumen is greater than zero
         to reduce the volume by a certain amount per frame*/
        if(vol > 0)
        {
            vol -= reducePerFrame;
        }
    }
}
