﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMovement : MonoBehaviour
{
    private Data data; /*reference to the Data script*/
    private CharacterController control; /*reference ot the CharacterController component*/
    [HideInInspector]
    public Transform transf; /*reference to the transform of the game object*/
    [HideInInspector]
    public Sound sound; /*reference to the Sound script of the game object*/

    void Start()
    {
        control = GetComponent<CharacterController>(); /*setting the control variable to get
                                                        the CharacterController component*/
        data = GetComponent<Data>(); /*setting the data variable to get the script on the
                                        game object*/
        transf = GetComponent<Transform>(); /*setting the transf variable to get the Transform
                                            component of the game object*/
        sound = GetComponent<Sound>(); /*setting the sound variable to get the Sond script
                                        on the game object*/
    }

    /*move method that uses the SimpleMove method to move the tank based off the movement vector
     multiplied by the movement speed of the tank*/
    public void Move(Vector3 movement)
    {
        control.SimpleMove(movement.normalized * data.moveSpeed);
    }

    /*turning method that using the Rotate method of the transform to rotate the tank on the 
     y-axis by the positive or negative sign of the direction float multiplied by the 
     turning speed of the tank multiplied by the time per frame draw*/
    public void Turning(float dir)
    {
        transf.Rotate(0, Mathf.Sign(dir) * data.turnSpeed * Time.deltaTime, 0);
    }

    /*method that has the tank look towards a target by rotating it*/
    public void LookTowards(Vector3 target)
    {
        Quaternion rotate = Quaternion.LookRotation(target);

        transf.rotation = Quaternion.RotateTowards(transf.rotation, rotate, data.turnSpeed * Time.deltaTime);
    }
}
