﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellForce : MonoBehaviour
{
    private Transform trans; //reference to the transform of the shell
    private ShellDamage data; //reference to the ShellDamage script to get the shell data

    void Start()
    {
        trans = GetComponent<Transform>();
        data = GetComponent<ShellDamage>();
    }

    void Update()
    {
        Force();
    }

    void Force()
    {
        trans.position += trans.forward * data.fireForce * Time.deltaTime;
    }
}
