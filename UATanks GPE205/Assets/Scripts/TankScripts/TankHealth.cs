﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankHealth : MonoBehaviour
{
    public Data data; /*reference to the Data script*/
    [HideInInspector]
    public float health; /*float variable holding the current health of the tank*/
    [HideInInspector]
    public GameObject player1;
    [HideInInspector]
    public GameObject player2;
    [HideInInspector]
    public GameManager manager;
    [HideInInspector]
    public GameObject enemy;

    public Image healthBar; /*reference to the Image of the health bar of the tank*/


    // Start is called before the first frame update
    void Start()
    {
        health = data.startHealth; /*setting the current equal to the starting health of the tank*/
        player1 = GameObject.FindGameObjectWithTag("Player1");
        player2 = GameObject.FindGameObjectWithTag("Player2");
        enemy = GameObject.FindGameObjectWithTag("Enemy");
        manager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    /*TakeDamage() uses the startHealth variable from the TankData script and calculates
     how much to fill the player tanks health bar based on the current health of the
     player tank*/
    public void TakeDamage(float damage)
    {
        data.startHealth -= damage;

        healthBar.fillAmount = health / data.startHealth;
        /*if statement destroys the player tank if the player's health reaches zero*/
        if(health <= 0)
        {
            Die();
        }

        if (player1 == null)
        {
            manager.SpawnPlayer1();
            manager.DecrementPlayer1Lives();
        }

        if(manager.multiPlayer == true && player2 == null)
        {
            manager.SpawnPlayer2();
            manager.DecrementPlayer2Lives();
        }

        if(enemy == null)
        {
            manager.SpawnEnemy();
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
