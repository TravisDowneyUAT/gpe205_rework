﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoints : MonoBehaviour
{
   [HideInInspector]
   public Data data; /*reference to the Data script*/

   public List<Transform> points; /*list object to hold waypoints*/
   public int curPoint; /*int variable to find the current waypoint*/
   public float closeEnough; /*float variable to tell when the tank is close enough to the
                               waypoint*/
   public bool isMoving = true; /*bool variable to tell if the tank is moving*/
   public PersonalityControl perCon; /*refernece to the PersonalityControl script*/

   public enum PointOrder { Loop, BackForth, Stop } /*enum list holding the different ways to go through the waypoints*/
   public PointOrder order; /*PointOrder variable t hold the current order the tank will move in*/
   public bool isAdvancing = true; /*bool variable to tell if the tank is advancing through the waypoints*/

   public bool showNextPoint = false; /*bool variable set to show the next waypoint*/

   void Start()
    {
        data = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Data>();
    }

    void Update()
    {
     /*if statement saying that if the tank is moving to have it switch to the Patrol state
     and run the Walk method*/
        if (isMoving)
        {
            Walk();
            perCon.curState = PersonalityControl.States.Patrol;
        }
    }

    /*Walk method that gets the target waypoint and the current position of the enemy tank
     then the calculates the distance from the tanks current position and has the 
     tank look towards the point and then move to point in the scene*/
    void Walk()
    {
        Vector3 targetPoint =
            new Vector3(points[curPoint].position.x,
           data.motor.transf.position.y,
            points[curPoint].position.z);

        Vector3 curPos = data.motor.transf.position;

        Vector3 toPoint = targetPoint - curPos;

        data.motor.LookTowards(toPoint);

        data.motor.Move(data.motor.transf.forward);

    /*if statement that says if the distance between the tank and the target point is
     less than or equal to the close enough variable to find the next waypoint*/
        if (Vector3.Distance(data.motor.transf.position, targetPoint) <= closeEnough)
        {
            FindNextPoint();
            //FindNextPointAdvanced();
        }
    }

    /*method to have the tank find the next waypoint by incrementing the current point variable
     to move through the list and then if the current point has gone above or equal to the 
     count of the list to reset it back to 0 and run through the list again by setting the
     current point to a min and max value of 0 to the count of the list minus 1*/
    void FindNextPoint()
    {
        curPoint++;
        if (curPoint >= points.Count)
        {
            curPoint = 0;
        }

        curPoint = Mathf.Clamp(curPoint, 0, points.Count - 1);
    }

    /*advanced method of finding the next waypoint depending on what state the tank is set to*/
    void FindNextPointAdvanced()
    {
        if (order == PointOrder.BackForth)
        {
            if (isAdvancing)
            {
                curPoint++;
            }
            else
            {
                curPoint--;
            }
        }
        else
        {
            curPoint++;
        }

        if (curPoint >= points.Count)
        {
            if (order == PointOrder.Loop)
            {
                curPoint = 0;
            }else if (order == PointOrder.BackForth)
            {
                isAdvancing = false;
                curPoint = curPoint - 1;
            }else if (order == PointOrder.Stop)
            {
                isMoving = false;
            }
        }else if (curPoint < 0)
        {
            isAdvancing = true;
            curPoint = 1;
        }
    }
}
