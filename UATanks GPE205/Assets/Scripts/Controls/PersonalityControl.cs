﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityControl : MonoBehaviour
{
    [HideInInspector]
    public Data data; /*reference to the Data script*/
    public enum ObsAvoid { Norm, Rotate, Move } /*enum list for obstacle avoidance*/
    public ObsAvoid avoid = ObsAvoid.Norm; /*obstacle avoidance variable holding the current avoidance state*/

    public enum States { Wait,ChaseFlee,Caution,Patrol,} /*enum list for FSM*/
    public States curState; /*FSM variable holding thecurrent state*/

    public float obsTime; /*float variable holding the time the tank is a state in the obstacle
                            avoidance list*/
    public float obsDur; /*float variable holding the duration of the */
    public float obsDis; /*float variable holding the distance to the obstacle*/

    public float cautionTimer; /*float variable holding a timer for the caution state*/
    public float cautionDis; /*float variable holding the disance the player has to be for the enemy
                                to still be in the caution state*/
    public float waitTime; /*float variable holding the time the tank spends in the wait state*/
    public float chaseDis; /*float variable holding the chase distance that will activate the chaseflee state*/
    public float fleeTime; /*float variable holdign the time the tanks spends fleeing from the player*/

    public float chaseFleeHealRate; /*float variable holding the rate at which the tank loses health while
                                        in the ChaseFlee state*/
    public float waitHealRate; /*float variable holding the rate at which the tank loses health while
                                    in the Wait state*/
    public float chaseFleeHealthPer; /*float variable holding the percentage of health that needs
                                        to be left for the tank to enter or exit the ChaseFlee state*/

    public float inCurrent; /*float variable holding the amount of time spent in the current state*/

    Transform player1; /*variable holding the first player's transform*/
    Transform self; /*variable holding the enemy tank's transform*/
    Transform player2; /*variable holding the second player's transform*/

    void Start()
    {
        data = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Data>();
    }

    // Update is called once per frame
    void Update()
    {
        /*Getting the player's transform by referencing the transform from the TankMovement script*/
        player1 = GameObject.FindGameObjectWithTag("Player1").GetComponent<Transform>();
        /*Getting the second player's transform by reference the transform from the TankMovement script*/
        player2 = GameObject.FindGameObjectWithTag("Player2").GetComponent<Transform>();
        /*Getting the enemy tank's transform by referencing the TankMovement script*/
        self = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Transform>();

        inCurrent += Time.deltaTime; /*setting the inCurrent variable to increment based on the time each frame*/

        /*switch statement running throught the different states of the FSM*/
        switch (curState)
        {
            /*case for the Wait state that says if the time spent in the current state is
             * greater than the wait time then to check to see if the current health of the tank
             multiplied by 100 and divided by the starting health is less than the percentage for
             the ChaseFlee state or if the distance between the enemy tank and the player 
             is less than the chase distance to switch to the ChaseFlee state*/
            case States.Wait:
                Wait();

                if (inCurrent > waitTime)
                {
                    if ((data.healthInfo.health * 100 / data.startHealth) < chaseFleeHealthPer
                        || Vector3.Distance(self.position, player1.position) < chaseDis && Vector3.Distance(self.position, player2.position) < chaseDis)
                    {
                        ShiftState(States.ChaseFlee);
                    }
                }
                break;

            /*case for the ChaseFlee state the says is the distance between the enemy tank and the
             player is less than the chase distance then run the Chase method or if the
             current health multiplied by 100 divded by the starting health is less than the 
             ChaseFlee percentage then run the Flee method or if the time spent in the current 
             state is greater than the time spent fleeing to change to the Wait state*/
            case States.ChaseFlee:
                if (Vector3.Distance(self.position, player1.position) < chaseDis || Vector3.Distance(self.position, player2.position) < chaseDis)
                {
                    Chase();
                }else if ((data.healthInfo.health * 100 / data.startHealth) < chaseFleeHealthPer)
                {
                    Flee();
                }else if(inCurrent > fleeTime)
                {
                    ShiftState(States.Wait);
                }
                break;
                
            /*case for the Caution state that says if the distance between the enemy tank
             and the player is less than the caution distance to shift the state to the
             ChaseFlee state or if the current health is less than or equal to the starting
             health and the timer for the caution state is greater than the time per frame
             to shift to the Patrol state*/
            case States.Caution:
                if (Vector3.Distance(self.position, player1.position) < cautionDis || Vector3.Distance(self.position, player2.position) < cautionDis)
                {
                    ShiftState(States.ChaseFlee);
                }else if (data.healthInfo.health >= data.startHealth && cautionTimer > Time.deltaTime)
                {
                    ShiftState(States.Patrol);
                }
                break;
            
            /*case for the Patrol state that says if the distance between the enemy tank
             and the player is less than the chase distance to shift to the
             ChaseFlee state*/
            case States.Patrol:
                if (Vector3.Distance(self.position, player1.position) < chaseDis || Vector3.Distance(self.position, player2.position) < chaseDis)
                {
                    ShiftState(States.ChaseFlee);
                }
                break;
        }

    }

    /*method that tells the current state to change to the next state
     and resets the time spent in the current state back to zero*/
    void ShiftState(States changeState)
    {
        curState = changeState;

        inCurrent = 0;
    }

    /*method that tells the enemy tank to chase after the player tank
     and decrements the health by ChaseFlee health rate multipled by the time per frame draw*/
    void Chase()
    {
        data.healthInfo.health -= chaseFleeHealRate * Time.deltaTime;
        
        MotorTowards(GameManager.manager.playerCon.p1Data.motor.transf.position);
        MotorTowards(GameManager.manager.playerCon.p2Data.motor.transf.position);
    }

    /*method that tells the enemy tank to run from the player tank and decrements the 
     health by the ChaseFlee health rate multiplied by the time per frame draw */
    void Flee()
    {
        data.healthInfo.health -= chaseFleeHealRate * Time.deltaTime;

        Vector3 toPlayer1 = GameManager.manager.playerCon.p1Data.motor.transf.position - data.motor.transf.position;
        Vector3 toPlayer2 = GameManager.manager.playerCon.p2Data.motor.transf.position - data.motor.transf.position;

        toPlayer1 = -1 * toPlayer1;
        toPlayer2 = -1 * toPlayer2;

        toPlayer1.Normalize();
        toPlayer2.Normalize();

        MotorTowards(toPlayer1);
        MotorTowards(toPlayer2);
    }

    /*empty method that allows the enemy tank to stop and wait*/
    void Wait()
    {
        //Do nothing
    }

    /*method that tell the enemy tank to move towards the player if they are the target
     and runs if statements to tell if the tank should go about avoid obstacles normally,
     rotate to avoid them, or move to avoid them*/
    void MotorTowards(Vector3 tar)
    {
        if (avoid == ObsAvoid.Norm)
        {
            if (CanMotorF())
            {
                Vector3 tarPos = new Vector3(tar.x,
                    data.motor.transf.position.y,
                    tar.z);

                Vector3 toPoint = tarPos - data.motor.transf.position;

                data.motor.LookTowards(toPoint);

                data.motor.Move(data.motor.transf.forward);
            }
            else
            {
                avoid = ObsAvoid.Rotate;
            }
        }else if (avoid == ObsAvoid.Rotate)
        {
            data.motor.Turning(1);

            if (CanMotorF())
            {
                avoid = ObsAvoid.Move;
                obsTime = Time.time;
            }
        }else if(avoid == ObsAvoid.Move)
        {
            if (CanMotorF())
            {
                data.motor.Move(data.motor.transf.forward);
            }
            else
            {
                avoid = ObsAvoid.Rotate;
            }

            if (Time.time >= obsTime + obsDur)
            {
                avoid = ObsAvoid.Norm;
            }
        }
    }

    /*bool method that says if the enemy tank detects an obstacle and how far away it is*/
    bool CanMotorF()
    {
        if (Physics.Raycast(data.motor.transf.position, data.motor.transf.forward, obsDis))
        {
            return false;
        }

        return true;
    }
}
