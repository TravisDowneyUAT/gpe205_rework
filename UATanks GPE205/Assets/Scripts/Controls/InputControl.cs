﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputControl : MonoBehaviour
{
    [HideInInspector]
    public Data p1Data; //reference to the Data script

    [HideInInspector]
    public Data p2Data;

    KeyCode forward;
    KeyCode backward;
    KeyCode turnLeft;
    KeyCode turnRight;
    KeyCode shooting;

    KeyCode forward2;
    KeyCode backward2;
    KeyCode turnLeft2;
    KeyCode turnRight2;
    KeyCode shooting2;

    public enum PlayerInput { WASD,IJKL} //enum list setting the keys for player input
    public PlayerInput input = PlayerInput.WASD; //variable to set the player input keys

    void Start()
    {
        p1Data = GameObject.FindGameObjectWithTag("Player1").GetComponent<Data>();
        p2Data = GameObject.FindGameObjectWithTag("Player2").GetComponent<Data>();
    }

    // Update is called once per frame
    void Update()
    {
        SetPlayerInput();

        /*if statements moving the tank based off of player input*/
        if (Input.GetKey(forward))
        {
            p1Data.motor.Move(p1Data.motor.transf.forward);
        }
        if (Input.GetKey(backward))
        {
            p1Data.motor.Move(-p1Data.motor.transf.forward);
        }
        if (Input.GetKey(turnLeft))
        {
            p1Data.motor.Turning(-1);
        }
        if (Input.GetKey(turnRight))
        {
            p1Data.motor.Turning(1);
        }
        if (Input.GetKey(shooting))
        {
            p1Data.shooting.Shoot();
        }

        if (Input.GetKey(forward2))
        {
            p2Data.motor.Move(p2Data.motor.transf.forward);
        }
        if (Input.GetKey(backward2))
        {
            p2Data.motor.Move(-p2Data.motor.transf.forward);
        }
        if (Input.GetKey(turnLeft2))
        {
            p2Data.motor.Turning(-1);
        }
        if (Input.GetKey(turnRight2))
        {
            p2Data.motor.Turning(1);
        }
        if (Input.GetKey(shooting2))
        {
            p2Data.shooting.Shoot();
        }
    }

    /*method that sets the player input based off the key map chosen*/
    void SetPlayerInput()
    {
        switch (input)
        {
            case PlayerInput.WASD:
                forward = KeyCode.W;
                backward = KeyCode.S;
                turnLeft = KeyCode.A;
                turnRight = KeyCode.D;
                shooting = KeyCode.Space;
                break;

            case PlayerInput.IJKL:
                forward2 = KeyCode.I;
                backward2 = KeyCode.K;
                turnLeft2 = KeyCode.J;
                turnRight2 = KeyCode.L;
                shooting2 = KeyCode.RightShift;
                break;
        }
    }
}
