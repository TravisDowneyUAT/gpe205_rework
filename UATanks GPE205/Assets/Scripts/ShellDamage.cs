﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellDamage : MonoBehaviour
{
    [HideInInspector]
    public TankHealth player;
    [HideInInspector]
    public TankHealth enemy;

    public float damage; /*float variable holding the damage that the shell will do 
                            if it hits a player or enemy*/
    public float lifeTime = 2.0f; /*float variable holding the life time of the shell 
                            once it is fired*/
    public float fireForce; /*float variable holding the force that will push the shell once it is fired */                       

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, lifeTime);
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<TankHealth>();
        enemy = GameObject.FindGameObjectWithTag("Enemy").GetComponent<TankHealth>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag.Equals("Player"))
        {
            player.TakeDamage(damage);

        }
        if (col.gameObject.tag.Equals("Enemy"))
        {
            enemy.TakeDamage(damage);
        }
    }
}
