﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sense : MonoBehaviour
{
    public PersonalityControl aiPersonality; //reference to the PersonalityControl script
    public float sight; //float variable measuring the distance the tank can see
    public float FOV; //float variable measuring the field of vision the tank has
    public float hearThresh; //float variable measuring how well the tank can hear
    [HideInInspector]
    public Sound player1Sound; //reference to the Sound script
    [HideInInspector]
    public GameObject player1; //reference to the player's game object
    [HideInInspector]
    public Transform player1Trans; //reference to the player's transform
    [HideInInspector]
    public GameObject player2;
    [HideInInspector]
    public Transform player2Trans;
    [HideInInspector]
    public Sound player2Sound;


    private Transform trans; //reference to the tank's transform
    
    // Start is called before the first frame update
    void Start()
    {
        trans = GetComponent<Transform>(); //setting the transf variable to the Transform component of the game object
        player1 = GameObject.FindGameObjectWithTag("Player1"); //setting the player game object to find the GameObject with the tag of Tank
        player1Sound = player1.GetComponent<Sound>(); //setting the playerSound variable to the Sound script of the player game object
        player1Trans = player1.GetComponent<Transform>(); //setting the playerTrans variable to the Transform of the player game object
        player2 = GameObject.FindGameObjectWithTag("Player2");
        player2Trans = player2.GetComponent<Transform>();
        player2Sound = player2.GetComponent<Sound>();

    }

    /*method that tells if the tank can sense the player by if the player's sound breaks the tanks
     hearing threshold*/
    public bool CanSense()
    {
        if (player1Sound == null || player2Sound == null)
        {
            return false;
        }

        if (Vector3.Distance(player1Trans.position, trans.position) <= player1Sound.vol * hearThresh 
            || Vector3.Distance(player2Trans.position, trans.position) <= player2Sound.vol * hearThresh)
        {
            return true;
        }

        return false;
    }

    /*OnTriggerEnter method that says if the player tank enters the sphere collider on the enemy tank
     to switch to the ChaseFlee state*/
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag.Equals("Player1"))
        {
            aiPersonality.curState = PersonalityControl.States.ChaseFlee;
        }

        if (col.gameObject.tag.Equals("Player2"))
        {
            aiPersonality.curState = PersonalityControl.States.ChaseFlee;
        }
    }

    /*OnTriggerExit method that says if the player exits the sphere collider on the enemy tank
     to switch to the Caution state*/
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag.Equals("Player1"))
        {
            aiPersonality.curState = PersonalityControl.States.Caution;
        }

        if (col.gameObject.tag.Equals("Player2"))
        {
            aiPersonality.curState = PersonalityControl.States.Caution;
        }
    }
}
