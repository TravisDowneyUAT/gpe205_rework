﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupControl : MonoBehaviour
{
    public Buffs buff;

    /*If the thing that rolls over the buff has a control script 
     * add that buff to the GameObject and destroy the buff*/
    void OnTriggerEnter(Collider col)
    {
        BuffControl buffCon = col.GetComponent<BuffControl>();

        if(buffCon != null)
        {
            buffCon.AddBuff(buff);
        }

        AudioManager.instance.PlaySound("Powerup");

        Destroy(gameObject);
    }
}
