﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffSpawner : MonoBehaviour
{
    public GameObject[] buffToSpawn; /*GameObject array to hold the buffs*/
    public GameObject spawnedBuff; /*Reference to the spawned buff in the scene*/

    private Transform transf;/*reference to the spawners transform*/
    public float delay; /*float variable holding the delay time for the next buff to spawn*/
    public float nxtSpawn; /*float variable holding the time for the next buff to spawn*/

    public Transform[] spawnerLocs; /*Transform array to hold to transforms of the different buff spawner locations*/

    void Start()
    {
        transf = GetComponent<Transform>();

        delay = nxtSpawn; 
    }

    void Update()
    {
        /*If there is no spawned buff then spawn one in at a random spawn point in the spawnerLocs array*/
        if (spawnedBuff == null)
        {
            delay -= Time.deltaTime;

            if (delay <= 0)
            {
                int locID = Random.Range(0, spawnerLocs.Length);
                int buffSpawn = Random.Range(0, buffToSpawn.Length);
                Transform spawnTransf = spawnerLocs[locID];
                GameObject spawnBuff = buffToSpawn[buffSpawn];

                spawnedBuff = Instantiate(spawnBuff, spawnTransf.position, spawnTransf.rotation) as GameObject;

                delay = nxtSpawn;
            }
        }
    }
}
