﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffControl : MonoBehaviour
{
    public List<Buffs> buffs;
    private Data data;

    // Start is called before the first frame update
    void Start()
    {
        data = GetComponent<Data>();
    }

    /*Adding expired buffs to a new list once their duration has reached zero*/
    void Update()
    {
        List<Buffs> expiredBuffs = new List<Buffs>();

        foreach (Buffs buff in buffs)
        {
            buff.buffDuration -= Time.deltaTime;
            if (buff.buffDuration <= 0)
            {
                expiredBuffs.Add(buff);
            }
        }

        foreach (Buffs buff in expiredBuffs)
        {
            RemoveBuff(buff);
        }
    }

    /*Adding buff to the tank based of which buff it is*/
    public void AddBuff(Buffs buffToAdd)
    {
        Buffs newBuff = new Buffs();

        if (buffToAdd.buffType == Buffs.BuffType.DamageBuff)
        {
            newBuff = new BuffDamage((BuffDamage)buffToAdd);
        }
        else if (buffToAdd.buffType == Buffs.BuffType.SmallShieldBuff)
        {
            newBuff = new BuffSmallShield((BuffSmallShield)buffToAdd);
        }
        else if (buffToAdd.buffType == Buffs.BuffType.MedShieldBuff)
        {
            newBuff = new BuffMedShield((BuffMedShield)buffToAdd);
        }else if (buffToAdd.buffType == Buffs.BuffType.LargeShieldBuff)
        {
            newBuff = new BuffLargeShield((BuffLargeShield)buffToAdd);
        }else if (buffToAdd.buffType == Buffs.BuffType.SpeedBuff)
        {
            newBuff = new BuffSpeed((BuffSpeed)buffToAdd);
        }
        newBuff.OnAdd(data);

        if (!newBuff.isPerm)
        {
            buffs.Add(newBuff);
        }
    }

    /*Removing the buff based on which buff has been added*/
    public void RemoveBuff(Buffs buffToRemove)
    {
        buffs.Remove(buffToRemove);
        buffToRemove.OnRemove(data);
    }

}
