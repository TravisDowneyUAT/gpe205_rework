﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class BuffMedShield : Buffs
{
    public float buffAmount;

    /*Adding the amounts to the cloned prefab from the base prefab*/
    public BuffMedShield(BuffMedShield buffToClone)
    {
        buffAmount = buffToClone.buffAmount;
        buffDuration = buffToClone.buffDuration;
        isPerm = buffToClone.isPerm;

        buffType = BuffType.MedShieldBuff;
    }

    /*Overriding the OnAdd data to change the health of the tank based on the buff amount*/
    public override void OnAdd(Data data)
    {
        data.healthInfo.health += buffAmount;

        base.OnAdd(data);
    }

    /*Overriding the OnRemove data to change the health of the tank after the buff has been removed*/
    public override void OnRemove(Data data)
    {
        base.OnRemove(data);

        data.healthInfo.health -= buffAmount;
    }
}
