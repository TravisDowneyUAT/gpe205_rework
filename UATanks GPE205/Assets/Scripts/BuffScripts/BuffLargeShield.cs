﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class BuffLargeShield : Buffs
{
    public float buffAmount;

    /*Changing the amounts of the clone prefab to match the amounts of the base prefab*/
    public BuffLargeShield(BuffLargeShield buffToClone)
    {
        buffAmount = buffToClone.buffAmount;
        buffDuration = buffToClone.buffDuration;
        isPerm = buffToClone.isPerm;

        buffType = BuffType.LargeShieldBuff;
    }

    /*Overriding the OnAdd data to increase the health based on the buff amount*/
    public override void OnAdd(Data data)
    {
        data.healthInfo.health += buffAmount;

        base.OnAdd(data);
    }

    /*Overriding the OnRemove data to decrease the health based on the buff amount after the buff is removed*/
    public override void OnRemove(Data data)
    {
        base.OnRemove(data);

        data.healthInfo.health -= buffAmount;
    }
}
