﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuffSpeed : Buffs
{
    public float buffAmount;

    /*Adding information to the cloned buff once it is spawned into the scene*/
    public BuffSpeed(BuffSpeed buffToClone)
    {
        buffAmount = buffToClone.buffAmount;
        buffDuration = buffToClone.buffDuration;
        isPerm = buffToClone.isPerm;

        buffType = BuffType.SpeedBuff;
    }

    /*Overriding the OnAdd data to increase the move speed by the amount on the buff once the buff is added*/
    public override void OnAdd(Data data)
    {
        data.moveSpeed += buffAmount;

        base.OnAdd(data);
    }

    /*Overriding the OnRemove data to decrease the move speed by the amount of the buff once the buff is removed*/
    public override void OnRemove(Data data)
    {
        base.OnRemove(data);

        data.moveSpeed -= buffAmount;
    }
}
