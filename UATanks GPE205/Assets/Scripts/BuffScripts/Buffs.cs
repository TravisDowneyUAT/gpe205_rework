﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class Buffs
{
    /*List of the buff types*/
    public enum BuffType
    {
        SpeedBuff,
        SmallShieldBuff,
        MedShieldBuff,
        LargeShieldBuff,
        DamageBuff
    }
    
    public float buffDuration; /*float variable for the duration the buff will be applied to the tank*/
    public bool isPerm; /*bool variable saying if the buff is premanent or not*/

    public Buffs() { }

    public BuffType buffType;

    public virtual void OnAdd(Data data) { }
    public virtual void OnRemove(Data data) { }
}
