﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuffSmallShield : Buffs
{
    public float buffAmount;

    /*Setting the amounts of the cloned prefab to the base prefabs amounts*/
    public BuffSmallShield(BuffSmallShield buffToClone)
    {
        buffAmount = buffToClone.buffAmount;
        buffDuration = buffToClone.buffDuration;
        isPerm = buffToClone.isPerm;

        buffType = BuffType.SmallShieldBuff;
    }

    /*Overring the OnAdd data to add to the tanks health based off the buff amount*/
    public override void OnAdd(Data data)
    {
        data.healthInfo.health += buffAmount;

        base.OnAdd(data);
    }

    /*Overriding the OnRemove to remove the buff amount from the tanks health*/
    public override void OnRemove(Data data)
    {
        base.OnRemove(data);

        data.healthInfo.health -= buffAmount;
    }
}
