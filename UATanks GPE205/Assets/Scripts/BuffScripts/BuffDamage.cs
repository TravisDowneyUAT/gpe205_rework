﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuffDamage : Buffs
{
    public float buffAmount;

    /*Changing the amounts on the clone prefab to match the amounts of the base prefab*/
    public BuffDamage(BuffDamage buffToClone)
    {
        buffAmount = buffToClone.buffAmount;
        buffDuration = buffToClone.buffDuration;
        isPerm = buffToClone.isPerm;

        buffType = BuffType.DamageBuff;
    }

    /*Overriding the OnAdd data to change the damage the tank deals based on the buff amount*/

    public override void OnAdd(Data data)
    {
        data.damage += buffAmount;

        base.OnAdd(data);
    }

    /*Overriding the OnRemove data to change the damage the tank deals if the buff is removed*/
    public override void OnRemove(Data data)
    {
        base.OnRemove(data);

        data.damage -= buffAmount;
    }
}
