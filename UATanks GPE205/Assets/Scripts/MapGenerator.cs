﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public int rows;
    public int columns;
    public float width;
    public float height;
    public GameObject[] gridPrefab;
    private Room[,] grid;

    public GameObject RandomRoom()
    {
        return gridPrefab[Random.Range(0, gridPrefab.Length)];
    }

    public void GenerateMap()
    {
        for (int r = 0; r < rows; r++)
        {
            for (int c = 0; c < columns; c++)
            {
                float xPos = width * c;
                float zPos = height * r;
                Vector3 newPos = new Vector3(xPos, 0.0f, zPos);

                GameObject roomObj = Instantiate(RandomRoom(), newPos, Quaternion.identity) as GameObject;

                roomObj.transform.parent = this.transform;

                roomObj.name = "Room_Yeet" + c + "," + r;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
