﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //Camera variables
    public Camera cam1;
    public Camera cam2;
    enum CamConfig { Vert, Horizon }
    CamConfig currentCamConfig;

    //Enemy tank variables
    public GameObject[] enemyTanks;
    public Transform enemySpawnLoc;
    public GameObject spawnedEnemy;

    //Player tank variables
    public InputControl playerCon;
    public GameObject player1;
    public GameObject player2;
    public Transform[] playerSpawn;
    [HideInInspector]
    public Vector3 playerSpawnPoint;

    //Map Generation Variables
    public int rows;
    public int columns;
    public float roomW = 50.0f;
    public float roomH = 50.0f;
    public GameObject[] roomPrefab;
    private Room[,] room;
    public int seed;
    [HideInInspector]
    public bool isMapOfTheDay = false; //bool for setting the map of the day

    //HiScore variables
    private int player1Score;
    private int player1HiScore;
    private int player1Lives;
    private int player2Score;
    private int player2HiScore;
    private int player2Lives;

    public Text player1ScoreText;
    public Text player2ScoreText;
    public Text player1LivesText;
    public Text player2LivesText;
    public Text player1HiScoreText;
    public Text player2HiScoreText;
    

    public static GameManager manager;

    [HideInInspector]
    public bool multiPlayer = false; //bool for setting up multiplayer

    void Awake()
    {
        if(manager == null)
        {
            manager = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        player1HiScore = PlayerPrefs.GetInt("Player 1 HiScore", 0); //setting the player's high score based on the previous game session
        player2HiScore = PlayerPrefs.GetInt("Player 2 HiScore", 0); //setting the second player's high score based on the previous game session

        /*If map of the day is set to true then generate the map of the day*/
        if (isMapOfTheDay == true)
        {
            DateTime today = DateTime.Now;

        }
        /*If random map is set to true then generate a random map*/
        else
        {
            UnityEngine.Random.InitState(seed);
        }
        StartGame();
        currentCamConfig = CamConfig.Horizon;
    }
    
    /*StartGame() spawns in the player tank and runs the SpawnEnemyTanks() method*/
   public void StartGame()
    {
        //setting player scores and lives
        player1Score = 0;
        player2Score = 0;
        player1Lives = 3;
        player2Lives = 3;

        //HUD
        player1ScoreText.text = "Player 1 Score: " + player1Score;
        player2ScoreText.text = "Player 2 Score: " + player2Score;
        player1HiScoreText.text = "Player 1 HiScore: " + player1HiScore;
        player2HiScoreText.text = "Player 2 HiScore: " + player2HiScore;
        player1LivesText.text = "Player 1 Lives: " + player1Lives;
        player2LivesText.text = "Player 2 Lives: " + player2Lives;

        GenerateMap();
        /*if statement saying that if multiplayer bool is set to true then enable the second camera
         and set the player2 game object to active*/
        if(multiPlayer == true)
        {
            player2.SetActive(true);
            cam2.enabled = true;
            PositionCams();
        }
        else
        {
            player2.SetActive(false);
            cam2.enabled = false;
        }
    }

    /*Decrement player 1's lives and display the counter on the UI*/
    public void DecrementPlayer1Lives()
    {
        player1Lives--;
        player1LivesText.text = "Player 1 Lives: " + player1Lives;
    }

    /*Decrement player 2's lives and display the counter on the UI*/
    public void DecrementPlayer2Lives()
    {
        player2Lives--;
        player2LivesText.text = "Player 2 Lives: " + player2Lives;
    }

    /*When both player's have lost their lives then load the GameOver scene which prompts
     the players to either restart or quit the game*/
    void LivesCount()
    {
        if(player1Lives < 1 && player2Lives < 1)
        {
            SceneManager.LoadScene("GameOver");
        }
    }

    /*Spawn the enemy tank at a random spawn point in the enemy spawn location array*/
    public void SpawnEnemy()
    {
        int enemySpawnID = UnityEngine.Random.Range(0, enemyTanks.Length);
        GameObject spawnEnemy = enemyTanks[enemySpawnID];

        spawnedEnemy = Instantiate(spawnEnemy, enemySpawnLoc.position, enemySpawnLoc.rotation) as GameObject;
    }

    /*Spawn the player at a random point in the player spawn location array*/
    public void SpawnPlayer1()
    {
        int playerSpawnID = UnityEngine.Random.Range(0, playerSpawn.Length);
        Transform playerSpawnTransf = playerSpawn[playerSpawnID];
        player1 = Instantiate(player1, playerSpawnTransf.position, playerSpawnTransf.rotation) as GameObject;
    }

    public void SpawnPlayer2()
    {
        int playerSpawnID = UnityEngine.Random.Range(0, playerSpawn.Length);
        Transform playerSpawnTransf = playerSpawn[playerSpawnID];
        player2 = Instantiate(player2, playerSpawnTransf.position, playerSpawnTransf.rotation);
    }
    public GameObject RandomRoom()
    {
        return roomPrefab[UnityEngine.Random.Range(0, roomPrefab.Length)];
    }

    /*Generate a random map*/
    public void GenerateMap()
    {
        room = new Room[columns, rows];

        Vector3 originOffset = new Vector3(roomW * (columns - 1) * 0.5f, 
                                    0.0f, 
                                    roomH * (rows - 1) * 0.5f);

        for (int r = 0; r < rows; r++)
        {
            for(int c = 0; c < columns; c++)
            {
                float xPos = roomW * c;
                float zPos = roomH * r;
                Vector3 newPos = new Vector3(xPos, 0.0f, zPos);

                newPos -= originOffset;

                CreateRoom(newPos, r, c);
            }
        }
    }

    public int DateToInt(DateTime dateToUse)
    {
        return dateToUse.Year + 
            dateToUse.Month + 
            dateToUse.Day + 
            dateToUse.Hour + 
            dateToUse.Minute + 
            dateToUse.Second + 
            dateToUse.Millisecond;
    }

    void CreateRoom(Vector3 pos, int r, int c)
    {
        GameObject roomObj = Instantiate(RandomRoom(), pos, Quaternion.identity);

        roomObj.transform.parent = this.transform;
        roomObj.name = "Room_One" + c + "," + r;

        Room createdRoom = roomObj.GetComponent<Room>();

        OpenDoors(createdRoom, r, c);

        room[c, r] = createdRoom;
    }

    /*Open the doors on the rooms depending on where they are in the rows and columns*/
    void OpenDoors(Room room, int r,int c)
    {
        if (r == 0)
        {
            room.doorNorth.SetActive(false);
        }else if (r == rows - 1)
        {
            room.doorSouth.SetActive(false);
        }
        else
        {
            room.doorNorth.SetActive(false);
            room.doorSouth.SetActive(false);
        }

        if (c == 0)
        {
            room.doorEast.SetActive(false);
        }else if (c == columns - 1)
        {
            room.doorWest.SetActive(false);
        }
        else
        {
            room.doorEast.SetActive(false);
            room.doorWest.SetActive(false);
        }
    }

    /*position the cameras based on the preferred split screen chosen by developers*/
    void PositionCams()
    {
        switch (currentCamConfig)
        {
            case CamConfig.Vert:
                cam1.rect = new Rect(0, 0, 0.5f, 1f);
                cam2.rect = new Rect(0.5f, 0, 0.5f, 1f);
                break;

            case CamConfig.Horizon:
                cam1.rect = new Rect(0, 0, 1f, 0.5f);
                cam2.rect = new Rect(0, 0.5f, 1f, 0.5f);
                break;
        }
    }
}
